package Java;
import Java.Constants;
import Java.TT;
import com.intel.bluetooth.BlueCoveImpl;

import org.json.JSONException;
import org.json.JSONObject;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.LoggingPermission;

import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.UUID;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
public class PCServerCOMM {
    static TT frame;
    private static StreamConnection mConnection;
    static OutputStream outStream;
    static InputStream inputStream;
    PrintWriter pWriter;
    Thread processThread;
    private static String uniqueKey;
    private static String dataToShow;
    static String totalMessage = "";
    static int totalChunk=0, receivedChunk=0;
    static boolean isSizeReceived = false;
    //start server
    public void startServer() throws IOException {

        boolean isRe = true;
        //Create a UUID for SPP
        //UUID uuid = new UUID("1101", true);
        UUID uuid = new UUID(Constants.K_UUID, false);
        //Create the servicve url
        String connectionString = Constants.KLOCAL_HOST + uuid + Constants.KSAMPLE_SERVER;

        //open server url
        StreamConnectionNotifier streamConnNotifier = (StreamConnectionNotifier) Connector.open(
                connectionString);

        //Wait for client connection
        System.out.println("\n"+Constants.KSERVER_STARTED);
        frame.setStatusText(Constants.KSERVER_STARTED);
        StreamConnection connection = streamConnNotifier.acceptAndOpen();

        RemoteDevice dev = RemoteDevice.getRemoteDevice(connection);
        System.out.println("Remote device address: " + dev.getBluetoothAddress());;
        frame.setStatusText("Device Mac Adress: " + dev.getBluetoothAddress() + " connected successfully" );
        frame.setBluetoothConnectionStatus(true);
        processThread = new Thread(new ProcessConnectionThread(connection));
        processThread.start();
//        InputStream inStream = connection.openInputStream();
//        BufferedReader bReader = new BufferedReader(new InputStreamReader(inStream));
//        while (true){
//            String lineRead = bReader.readLine();
//            System.out.println(lineRead);
//        }
//        System.out.println("Remote device name: " + dev.getFriendlyName(true));

        //read string from spp client
//        InputStream inStream = connection.openInputStream();
//        BufferedReader bReader = new BufferedReader(new InputStreamReader(inStream));
//        String lineRead = bReader.readLine();
//        System.out.println(lineRead);

        //send response to spp client
//        OutputStream outStream = connection.openOutputStream();
//        BufferedWriter bWriter = new BufferedWriter(new OutputStreamWriter(outStream));
//        bWriter.write("Response String from SPP Server\r\n");

        /*PrintWriter pWriter=new PrintWriter(new OutputStreamWriter(outStream));
        pWriter.write("Response String from SPP Server\r\n");
        pWriter.flush();
        pWriter.close();*/

        //streamConnNotifier.close();


    }
    public void writeDatatoCLient(String data){
        if(mConnection!=null){
            try {
                //String output = data.substring(0,1).toUpperCase() + data.substring(1);
                pWriter=new PrintWriter(new OutputStreamWriter(outStream));
                pWriter.write(data);
                pWriter.flush();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws IOException {


        frame = new TT();
        frame.setTitle(Constants.KAPP_TITLE);
        frame.setSize(800, 400);
        frame.setMinimumSize(new Dimension(800, 400));
        frame.setLocationRelativeTo(null); // Center the frame
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setStatusText(Constants.KSERVER_NOT_CONNECTED);
        frame.setBluetoothConnectionStatus(false);
        PCServerCOMM mSampleServer = new PCServerCOMM();
        mSampleServer.startBluetootServer();
        //display local device address and name

    }
    public class ProcessConnectionThread implements Runnable {


        public ProcessConnectionThread(StreamConnection connection)
        {
            mConnection = connection;
        }

        @Override
        public void run() {
            try {
                // prepare to receive data
                inputStream = mConnection.openInputStream();
                outStream = mConnection.openOutputStream();
                //BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream));
                System.out.println("waiting for input");
                byte[] s = new byte[1024];
                while (true) {
                    //int command = inputStream.read();
                    int bt = inputStream.read(s);
                    if(bt != -1){
                        String incomingMessage = new String(s, 0, bt);
                        if(!isSizeReceived){
                            totalChunk = Integer.parseInt(incomingMessage);
                            if(totalChunk == 0){
                                totalMessage = "";
                                writeDataFromClient();
                            }
                            else {
                                isSizeReceived = true;
                            }
                        }
                        else {
                            totalMessage += incomingMessage;
                            receivedChunk +=bt;
                            if(receivedChunk >= totalChunk){
                                //Total message received
                                writeDataFromClient();

                            }
                        }
                        //String lineRead = line;
//                        totalMessage += incomingMessage;
//                        if(inputStream.available()==0){
//
//                            frame.setIsTextFromClient(true);
//                            frame.setIsRemoveTextFromClient(true);
//                            frame.setSpeechText(totalMessage);
//                            totalMessage = "";
//                        }
//                        else {
//                            Thread.sleep(300);
//                        }
                        //String textToPrint = appendDataAccordingToLength(incomingMessage);
//                        System.out.println(textToPrint);
//                        frame.setIsTextFromClient(true);
//                        frame.setIsRemoveTextFromClient(true);
//                        frame.setSpeechText(textToPrint);
//                    }
                    }
                    else {
                        JOptionPane.showMessageDialog(null, Constants.KBLUETOOTH_CONNECTION_LOST);
                        stopServer();
                        frame.btn_connect.setText(Constants.KSTART_SERVER);
                        frame.setStatusText(Constants.KSERVER_NOT_CONNECTED);
                        frame.setBluetoothConnectionStatus(false);
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private void writeDataFromClient(){
        frame.setIsTextFromClient(true);
        frame.setIsRemoveTextFromClient(true);
        frame.setSpeechText(totalMessage);

        //Reset All
        isSizeReceived = false;
        totalChunk = 0;
        receivedChunk = 0;
        totalMessage = "";
    }
    public void stopServer(){
        try {
            if(inputStream!=null){
                inputStream.close();
            }
            if(outStream != null){
                outStream.close();
            }
            if(pWriter!=null){
                pWriter.close();
            }
            if(mConnection!=null){
                mConnection.close();
            }
            BlueCoveImpl.shutdown();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void startBluetootServer(){
        if(LocalDevice.isPowerOn()){
            // override the run() for the running behaviors
            try{
                LocalDevice localDevice = LocalDevice.getLocalDevice();
                System.out.println("Address: " + localDevice.getBluetoothAddress());
                System.out.println("Name: " + localDevice.getFriendlyName());
                frame.setButtonStatusText(Constants.KSTOP_SERVER);
                this.startServer();
            }
            catch (Exception localeException){
                localeException.printStackTrace();
            }
        }
        else {
            JOptionPane.showMessageDialog(null, Constants.KBLUETOOTH_ERROR);
        }
    }
    public String appendDataAccordingToLength(String data)
    {
        String key = "";
        JSONObject jsonObject = new JSONObject(data);
        Iterator<String> keys = jsonObject.keys();
        if( keys.hasNext() ){
          key = (String)keys.next(); // First key in your json object
        }
        String text = jsonObject.getString(key);
        if (key.equals(uniqueKey))
        {
         dataToShow = dataToShow + " " + text;
         uniqueKey = key;
        }
        else {
            uniqueKey = key;
            dataToShow = text;
        }
        return dataToShow;

    }

}
