/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Java;

/**
 *
 * @author Junaid
 */

public class Constants {
    public static String K_UUID = "0000110100001000800000805F9B34FB";
    public static String KAPP_TITLE = "Speech Mirror";
    public static String KLOCAL_HOST = "btspp://localhost:";
    public static String KSAMPLE_SERVER = ";name=Sample SPP Server";
    public static String KSTART_SERVER = "Start Server";
    public static String KSTOP_SERVER = "Stop Server";
    public static String KCOPY_CLIPBOARD = "Copy to Clipboard";
    public static String KPRINT_TEXT = "Print";
    public static String KSAVE_TEXT = "Save";
    public static String KCONNECTION_STATUS = "Connection Status:   ";
    public static String KBLUETOOTH_ERROR = "Please turn on the Bluetooth";
    public static String KBLUETOOTH_CONNECTION_LOST = "Bluetooth connection lost. You need to connect again";
    public static String KSERVER_NOT_CONNECTED = "Server not connected to any device";
    public static String KSERVER_STARTED = "Server Started. Waiting for clients to connect...";
}
