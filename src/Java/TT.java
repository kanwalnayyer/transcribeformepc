/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Java;

import com.intel.bluetooth.BlueCoveImpl;
//import com.sun.tools.javac.util.GraphUtils;

import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.BufferedWriter;
import java.io.Console;
import java.io.File;
import java.io.FileWriter;
import java.util.*;
import java.util.logging.ConsoleHandler;


import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.standard.MediaPrintableArea;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.OrientationRequested;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.xml.soap.Text;
import javax.xml.ws.handler.Handler;

import static java.lang.Math.abs;
import static java.lang.Math.min;
import static java.lang.Math.pow;
import static java.lang.Math.random;
import static java.lang.Math.round;
import javax.swing.filechooser.FileSystemView;
import static org.apache.commons.lang3.StringUtils.leftPad;


public class TT extends JFrame {

    JPanel top_panel, connectivity_panel;
    JLabel label, status_light, connection_status_text;
    ImageIcon mImageIcon_red, mImageIcon_green;
    JButton btn_connect;
    JButton btn_copy,btn_print,btn_save;
    JTextArea bluetoothText;
    JLabel statusText;
    LocalDevice localDevice;
    PCServerCOMM sampleSPPServer;
    boolean isTextFromClient = false;
    boolean isRemoveTextFromClient = false;
    java.util.Timer mTimer;
    String data;

    public TT() {
        intializeViews();
        addListeners();

//Print Functionality
        //<--------------->
//        final JButton btn_print = new JButton("Print");
//        connectivity_panel.add(btn_print);
//        btn_print.addActionListener(new ActionListener(){
//            public void actionPerformed(ActionEvent e){
//                printToPrinter(bluetoothText.getText().toString());
//            }
//        });
        //<--------------->

    }
    private void intializeViews(){
        sampleSPPServer = new PCServerCOMM();
        Image image = new ImageIcon(getClass().getClassLoader().getResource("icons/ic_launcher.png")).getImage();
        setIconImage(image);
        top_panel = new JPanel();
        add(top_panel);
        mImageIcon_red = new ImageIcon(getClass().getClassLoader().getResource("icons/red_light.png"));
        mImageIcon_green = new ImageIcon(getClass().getClassLoader().getResource("icons/green_light.png"));
        top_panel.setLayout(new BoxLayout(top_panel, BoxLayout.Y_AXIS));
        top_panel.add(Box.createVerticalStrut(20));
        connection_status_text = new JLabel(Constants.KCONNECTION_STATUS);
        status_light = new JLabel();
        status_light.setIcon(mImageIcon_red);
        status_light.setBorder(new EmptyBorder(0, 0, 0, 10));
        connectivity_panel = new JPanel();
        connectivity_panel.setLayout(new BoxLayout(connectivity_panel, BoxLayout.X_AXIS));
        top_panel.add(connectivity_panel);
        statusText = new JLabel();
        connectivity_panel.add(connection_status_text);
        connectivity_panel.add(status_light);
        btn_connect = new JButton(Constants.KSTART_SERVER);
        connectivity_panel.add(btn_connect);
        btn_copy = new JButton(Constants.KCOPY_CLIPBOARD);
        connectivity_panel.add(btn_copy);
        btn_print = new JButton(Constants.KPRINT_TEXT);
        connectivity_panel.add(btn_print);
        btn_save = new JButton(Constants.KSAVE_TEXT);
        connectivity_panel.add(btn_save);
        top_panel.add(Box.createVerticalStrut(20));
        bluetoothText = new JTextArea();
        bluetoothText.setWrapStyleWord(true);
        bluetoothText.setLineWrap(true);
        bluetoothText.setMargin(new Insets(0,10,0,10));
        JScrollPane scroll = new JScrollPane(bluetoothText);//place the JTextArea in a scroll pane
        top_panel.add(scroll, BorderLayout.CENTER);
    }
    private void addListeners(){
        btn_connect.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(btn_connect.getText().equals(Constants.KSTART_SERVER)){
                    if(LocalDevice.isPowerOn()){
                        Thread t = new Thread() {
                            @Override
                            public void run() {  // override the run() for the running behaviors
                                try{
                                    localDevice = LocalDevice.getLocalDevice();
                                    System.out.println("Address: " + localDevice.getBluetoothAddress());
                                    System.out.println("Name: " + localDevice.getFriendlyName());

                                    sampleSPPServer.startServer();
                                }
                                catch (Exception localeException){
                                    localeException.printStackTrace();
                                }
                            }
                        };
                        t.start();
                        btn_connect.setText(Constants.KSTOP_SERVER);
                    }
                    else {
                        JOptionPane.showMessageDialog(null, Constants.KBLUETOOTH_ERROR);
                    }
                }
                else {
                    sampleSPPServer.stopServer();
                    btn_connect.setText(Constants.KSTART_SERVER);
                    setStatusText(Constants.KSERVER_NOT_CONNECTED);
                    setBluetoothConnectionStatus(false);
                }
            }
        });
        btn_copy.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                StringSelection stringSelection = new StringSelection(bluetoothText.getText().toString());
                Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
                clpbrd.setContents(stringSelection, null);
            }
        });
        btn_print.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                printing();
            }
        });
        btn_save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
             Save();
            }
        });
        bluetoothText.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void removeUpdate(DocumentEvent e) {
                if(!isRemoveTextFromClient && sampleSPPServer!=null) {
                    if (mTimer != null) {
                        mTimer.cancel();
                        mTimer.purge();
                    }
                    mTimer = new java.util.Timer();
                    mTimer.schedule(
                            new java.util.TimerTask() {
                                @Override
                                public void run() {
                                    String data = bluetoothText.getText().toString();
                                    String capitalString = capitalizeString(data);
                                    checkDataLength(capitalString);
                                }
                            },
                            1500
                    );
                }
                else{
                    setIsRemoveTextFromClient(false);
                }

            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                if(!isTextFromClient && sampleSPPServer!=null){
                    if(mTimer!=null){
                        mTimer.cancel();
                        mTimer.purge();
                    }
                    mTimer = new java.util.Timer();
                    mTimer.schedule(
                            new java.util.TimerTask() {
                                @Override
                                public void run() {
                                    String data = bluetoothText.getText().toString();
                                   String capitalString = capitalizeString(data);
                                   isRemoveTextFromClient = true;
                                   isTextFromClient = true;
                                  //  isTextFromClient = true;
                                  //  bluetoothText.setText(capitalString);
                                   // bluetoothText.setCaretPosition(capitalString.length());
                                   // sampleSPPServer.writeDatatoCLient(sb.toString());
                                    //isTextFromClient = true;
                                    //bluetoothText.setText(sb.toString());
                                    //bluetoothText.setCaretPosition(sb.length());
                                   // sampleSPPServer.writeDatatoCLient(sb.toString());
                                    checkDataLength(capitalString);
                                    setSpeechText(capitalString);

                                     //  setSpeechText(sb.toString());

                                }
                            },
                            1500
                    );
                }
                else {
                    setIsTextFromClient(false);
                    setIsRemoveTextFromClient(false);
                }
            }

            @Override
            public void changedUpdate(DocumentEvent arg0) {

            }
        });
    }
    public void setSpeechText(String text){
        bluetoothText.setText(text);
        bluetoothText.setCaretPosition(bluetoothText.getDocument().getLength());
    }
    public void setStatusText(String text){
        statusText.setText(text);
    }
    public void setBluetoothConnectionStatus(Boolean status)
    {
        if(status){
            status_light.setIcon(mImageIcon_green);
        }
        else {
            status_light.setIcon(mImageIcon_red);
        }
    }
    public void setIsTextFromClient(boolean status){
        this.isTextFromClient = status;
    }
   
    public void setIsRemoveTextFromClient(boolean status){
        this.isRemoveTextFromClient = status;
    }

    private void printToPrinter(String printData)
    {
        PrinterJob job = PrinterJob.getPrinterJob();
        job.setPrintable(new OutputPrinter(printData));
        boolean doPrint = job.printDialog();
        if (doPrint)
        {
            try
            {
                job.print();
            }
            catch (PrinterException e)
            {
                // Print job did not complete.
            }
        }
    }

    public void setButtonStatusText(String text){
        btn_connect.setText(text);
    }

    public void checkDataLength(String data){
        sampleSPPServer.writeDatatoCLient(String.valueOf(data.getBytes().length));
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        sampleSPPServer.writeDatatoCLient(data);
//        int packetSize = 0;
//        int maxLenghSend = 958;
//        int stringLength = data.length(); // 2272
//        int initial = 0;
//        int maxLength;
//        maxLength = maxLenghSend;
//        String uid = generateUniqueId(5);
//        JSONObject jsonObject = new JSONObject();
//        if (stringLength % maxLenghSend == 0) {
//            packetSize = stringLength / maxLenghSend;
//        } else {
//            packetSize = (stringLength / maxLenghSend) + 1; // packetsize = 3
//        }
//
//            for (int i = 0; i < packetSize; i++) {    // i =0 , i = 1 ,i = 2
//                if (stringLength < maxLenghSend) //false , false ,true
//                {
//                    jsonObject.put(uid, data.substring(initial, (initial + stringLength))); // false , false , initial = 1902,stringlength = 1902 + 371
//                } else {
//                    jsonObject.put(uid, data.substring(initial, maxLength)); // initial =0 maxlength = 950 , initial =951 maxlength = 1901
//                }
//
//                stringLength = data.length() - maxLength; // 2272-950 = 1322 , 2272 - 1901=371
//                initial = maxLength; // initial = 950+1=951 , initial = 1901 +1 = 1902
//                maxLength = initial + maxLenghSend; // maxlength = 951 +950 =1901 , 1902 + 950=2852
//                sampleSPPServer.writeDatatoCLient(jsonObject.toString());
//                Thread.sleep(700);
//            }
    }

    public String generateUniqueId(int length)
    {

        StringBuffer sb = new StringBuffer();
        for (int i = length; i > 0; i -= 12) {
            int n = min(12, abs(i));
            sb.append(leftPad(Long.toString(round(random() * pow(36, n)), 36), n, '0'));
        }
        return sb.toString();
    }

    public String capitalizeString(String text)
    {
        int pos = 0;
        boolean capitalize = true;
        StringBuilder sb = new StringBuilder(text);
        while (pos < sb.length()) {
            if (sb.charAt(pos) == '.' || sb.charAt(pos) == '!' || sb.charAt(pos) == '?') {
                capitalize = true;
            } else if (capitalize && !Character.isWhitespace(sb.charAt(pos))) {
                sb.setCharAt(pos, Character.toUpperCase(sb.charAt(pos)));
                capitalize = false;
            }
            pos++;
        }

        String capitalized = WordUtils.capitalize(sb.toString(), '.', ' ');
        System.out.println(capitalized);
        return sb.toString();
    }
  
    public void Save()
    {

        String text = bluetoothText.getText();
        JFileChooser chooser = new JFileChooser();
        File home = FileSystemView.getFileSystemView().getHomeDirectory();
        chooser.setCurrentDirectory(home);
      //  chooser.setCurrentDirectory( new File( "./") );
        int actionDialog = chooser.showSaveDialog(this);
        if (actionDialog == JFileChooser.APPROVE_OPTION)
        {
            File fileName = new File(chooser.getSelectedFile( ) + ".txt" );
            if(fileName == null)
                return;
            if(fileName.exists())
            {
                actionDialog = JOptionPane.showConfirmDialog(this,
                        "Replace existing file?");
                if (actionDialog == JOptionPane.NO_OPTION)
                    return;
            }
            try
            {
                //StringBuilder sb = new StringBuilder(text);
                String separator = System.lineSeparator();
                BufferedWriter out = new BufferedWriter(new FileWriter(fileName));
                out.write(text.replace("\n","\r\n"));
                out.close();
            }
            catch(Exception e)
            {
                System.err.println("Error: " + e.getMessage());
            }
        }

    }
    public void printing()
    {
        
        JDialog dialog = new JDialog();
        dialog.setModal(true);
        dialog.setSize(700, 400);
        dialog.setLayout(new BorderLayout());
        Image image = new ImageIcon(getClass().getClassLoader().getResource("icons/ic_launcher.png")).getImage();
        dialog.setIconImage(image);
        HashPrintRequestAttributeSet set = new HashPrintRequestAttributeSet();
        set.add(MediaSizeName.ISO_A4);
        set.add(OrientationRequested.PORTRAIT);
        set.add(new MediaPrintableArea(50f, 50f, 50f, 50f, MediaPrintableArea.INCH));
        PageFormat pf = PrinterJob.getPrinterJob().getPageFormat(set);
        //PageFormat can be also prompted from user with PrinterJob.pageDialog()
       
        if (bluetoothText.getText().trim().equals("")) {
            JOptionPane.showMessageDialog(null, "Please enter some text first!"); 
        }
        else{
             final PrintPreview preview = new PrintPreview(bluetoothText.getPrintable(null, null), pf);

        JButton printButton = new JButton("Print");
        printButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                preview.print();
            }
        });
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.add(printButton);
        buttonsPanel.add(preview.getControls());
        dialog.getContentPane().add(preview, BorderLayout.CENTER);
        dialog.getContentPane().add(buttonsPanel, BorderLayout.SOUTH);
        dialog.setVisible(true);
    }
        }
}
